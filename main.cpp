#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "tree.h"
#include "word.h"
using namespace std;

int tokenizeAndInput(string, int); // Tokenize and Input | Return the amount of words inputted

vector<Word> vec; //Global var; should've used a better function but nahhhh

int main()
{
	fstream fs; //read from text file
	fs.open("input.txt");
	string line; //read from text file line-by-line
	int Nline = 1, count = 0;
	while(getline(fs,line))
	{
		/*
			While the fstream reads from text file line-by-line
			- tokenize the line into words, push back to the vector
			- increase word count and line count
		*/
		count += tokenizeAndInput(line,Nline);
		Nline++;
	}
	fs.close(); //end of fstream duty. good job *pat pat*
	
	Tree<Word> mytree;
	for(int i = 0; i < count; i++)
	{
		/*
			Insert each word from vector to the tree
		*/
		mytree.insert(vec[i]);
	}
	mytree.inorder(); //print all words using inorder traversal
	return 0;
}

int tokenizeAndInput(string inp, int line)
{
	/*
		This is manual tokenization
		variable index will find a blank space until the end of string
		for each blank space found, copy the chunck (from startInd to index) to the vector
		bump count up by 1
		send variable startInd to the position of index +1 (next chracter)
		return count after everything is done
	*/
	int count = 0, length;
	size_t startInd = 0, index = 0;
	index = inp.find_first_of(" ", startInd);
	char end[100];
	while(index != string::npos)
	{	
		length = inp.copy(end, index - startInd, startInd);
		end[length] = '\0';
		string str(end);
		vec.push_back(Word(str,line));
		startInd = index + 1;
		count++;
		index = inp.find_first_of(" ", startInd);
	}
	
	length = inp.copy(end, string::npos - startInd, startInd);
	end[length] = '\0';
	string str(end);
	vec.push_back(Word(str,line));
	startInd = index + 1;
	count++;
	
	return count;
}
