#include <string>

/*
	Word contains two variables
	- s: contains the word
	- line: contains the line position of the word
*/

class Word
{
	public:
		Word() { s = "-1"; line = -1; } //Default constructor
		Word(string n, int m) { s = n; line = m; }
		//accessor
		//by the way, you can't manually set it
		int getLine() { return line; }
		string getWord() { return s; }
		
		//override < for the sake of comparison
		bool operator<(const Word& c)
		{
			return this->s < c.s;	
		}
		 
	private:
		string s;
		int line;
};
